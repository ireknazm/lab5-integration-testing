SQR Lab 5
===

Irek Nazmiev, B17-SE-02



## Spec

Budet car price per minute = 15
Luxury car price per minute = 32
Fixed price per km = 14
Allowed deviations in % = 12
Inno discount in % = 17



## BVA

* Type: `"budget"`, `"luxury"`, `"nonsense"`
* Plan: `"minute"`, `"fixed_price"`, `"nonsense"`
* Distance: -10, 0, 1, 100
* Planned distance: -10, 0, 1, 100
* Time: -10, 0, 1, 10
* Planned time: -1, 0, 1, 10
* Discount: `"yes"`, `"no"`, `"nonsense"`



## Decision table

| Conditions       | R1       | R2       | R3       | R4          | R5          | R6          | R7          | R8          | R9          | R10         | R11         | R12                                   | R13                                   | R14     | R15    | R16    | R17    | R18    |
| ---------------- | -------- | -------- | -------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ------------------------------------- | ------------------------------------- | ------- | ------ | ------ | ------ | ------ |
| Type             | nonsense | *        | *        | luxury      | budget      | budget      | budget      | budget      | budget      | budget      | budget      | budget                                | budget                                | *       | budget | budget | luxury | luxury |
| Plan             | *        | nonsense | *        | fixed_price | fixed_price | fixed_price | fixed_price | fixed_price | fixed_price | fixed_price | fixed_price | fixed_price                           | fixed_price                           | minute  | minute | minute | minute | minute |
| Discount         | *        | *        | nonsense | *           | *           | *           | *           | *           | *           | *           | no          | no                                    | yes                                   | *       | no     | yes    | no     | yes    |
| Distance         | *        | *        | *        | *           | -10         | *           | *           | *           | 0           | 0           | 0           | *                                     | *                                     | *       | *      | *      | *      | *      |
| Planned distance | *        | *        | *        | *           | *           | -10         | *           | *           | 10          | 0           | 100         | *                                     | *                                     | *       | *      | *      | *      | *      |
| Time             | *        | *        | *        | *           | *           | *           | -10         | *           | 0           | 0           | 0           | 10                                    | 10                                    | -10     | 10     | 10     | 10     | 10     |
| Planned time     | *        | *        | *        | *           | *           | *           | *           | -10         | 0           | 0           | 0           | *                                     | *                                     | *       | *      | *      | *      | *      |
| **Results**      |          |          |          |             |             |             |             |             |             |             |             |                                       |                                       |         |        |        |        |        |
| Expected         | Invalid  | Invalid  | Invalid  | Invalid     | Invalid     | Invalid     | Invalid     | Invalid     | 140         | 0           | 1400        | Any other value except the actual one | Any other value except the actual one | Invalid | 150    | 124.5  | 320    | 265.6  |
| Actual           | Invalid  | Invalid  | Invalid  | Invalid     | Valid       | Valid       | Invalid     | Valid       | 0           | 0           | 0           | 166.66666666666666                    | 130                                   | Invalid | 150    | 124.5  | 416    | 324.48 |



## Bugs

1. Negative distance on fixed price plan is cosidered valid
2. Nagative planned distance on fixed plan is considered valid
3. Negative planned time on fixed plan is considered valid
4. In fixed plan only time and discount are counted (any value of distance, planned distance, planned time have zero effect on final value)
5. The price for luxury type minute plan is calculated incorrectly (most likely incorrect spec value or formula).
